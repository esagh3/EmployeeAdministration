create table employee (
    id bigint auto_increment NOT NULL ,
    job_area varchar(255) NOT NULL ,
    editing_date timestamp(6),
    email varchar(255) NOT NULL UNIQUE ,
    employee_id varchar(255) NOT NULL,
    employment_country varchar(255) NOT NULL,
    entry_date timestamp(6) NOT NULL,
    first_last_name varchar(255) NOT NULL,
    first_name varchar(255) NOT NULL,
    id_type varchar(255) NOT NULL ,
    other_name varchar(255),
    second_last_name varchar(255) NOT NULL ,
    employee_state varchar(255) NOT NULL ,
    primary key (id));

INSERT INTO EMPLOYEE (first_last_name, second_last_name, first_name, other_name, employment_country,
                      id_type, employee_id, email, employee_state, entry_date, editing_date, job_area)
VALUES
    ('CARRILLO','MENDOSA','SAMUEL',NULL,'COLOMBIA','CEDULA_DE_CIUDADANIA','726812',
     'samuel.carrillo@cidenet.com.co','ACTIVE','2012-09-17 18:47:52.069',NULL,'PURCHASE'),
    ('MURILLO','SOZA','ANA','MARIA','COLOMBIA','CEDULA_DE_CIUDADANIA','9269407','ana.murillo@cidenet.com.co',
     'ACTIVE','2022-12-31 18:47:52.069',NULL,'OPERATIONS');
package com.cidenet.employeeadministration.controller;

import com.cidenet.employeeadministration.entity.Employee;
import com.cidenet.employeeadministration.management.ValidationError;
import com.cidenet.employeeadministration.repository.EmployeeRepository;
import com.cidenet.employeeadministration.service.EmployeeService;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class EmployeeController {

    EmployeeService _employeeService;

    @Autowired
    public void setEmployeeRepository(EmployeeService employeeService){
        _employeeService = employeeService;
    }

    @GetMapping("/employees")
    ResponseEntity<List<Employee>> listEmployees() {
        List<Employee> employees = _employeeService.showAllEmployees();
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @PutMapping("/employees")
    ResponseEntity<?> addNewEmployee(@RequestBody Employee employee) {

        Either<List<ValidationError>,Employee> petitionResponse = _employeeService.registerEmployee(employee);

        if(petitionResponse.isRight()){
            return new ResponseEntity<>(petitionResponse.get(),HttpStatus.CREATED);
        }

        List<String> errors = new ArrayList<>();

        for (ValidationError error : petitionResponse.getLeft()){
            errors.add(error.toString());
        }

        return new ResponseEntity<>(errors,HttpStatus.BAD_REQUEST);
    }
    @PutMapping("/employees/{id}")
    ResponseEntity<?> editEmployee(@RequestBody Employee employee,@PathVariable Long id) {

        Either<List<ValidationError>,Employee> petitionResponse = _employeeService.editEmployee(id,employee);

        if(petitionResponse.isRight()){
            return new ResponseEntity<>(petitionResponse.get(),HttpStatus.ACCEPTED);
        }
        List<String> errors = new ArrayList<>();

        for (ValidationError error : petitionResponse.getLeft()){
            errors.add(error.toString());
        }

        return new ResponseEntity<>(errors,HttpStatus.BAD_REQUEST);
    }
    @DeleteMapping("/employees/{id}")
    ResponseEntity<?> deleteEmployee(@PathVariable Long id) {

        Either<List<ValidationError>,Employee> petitionResponse = _employeeService.deleteEmployeeById(id);

        if(petitionResponse.isRight()){
            return new ResponseEntity<>(petitionResponse.get(),HttpStatus.OK);
        }

        List<String> errors = new ArrayList<>();

        for (ValidationError error : petitionResponse.getLeft()){
            errors.add(error.toString());
        }

        return new ResponseEntity<>(errors,HttpStatus.BAD_REQUEST);
    }
}

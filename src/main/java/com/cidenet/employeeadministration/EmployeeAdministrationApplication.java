package com.cidenet.employeeadministration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
public class EmployeeAdministrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmployeeAdministrationApplication.class, args);
    }

}

package com.cidenet.employeeadministration.service;

import com.cidenet.employeeadministration.entity.Employee;
import com.cidenet.employeeadministration.management.ValidationError;
import io.vavr.control.Either;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface EmployeeService {
     Either<List<ValidationError>,Employee> registerEmployee(Employee employee);
     Either<List<ValidationError>,Employee> editEmployee(Long id, Employee employee);
     List<Employee> showAllEmployees();

     Either<List<ValidationError>,Employee> deleteEmployeeById(Long id);
}

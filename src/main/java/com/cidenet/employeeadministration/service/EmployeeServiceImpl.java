package com.cidenet.employeeadministration.service;

import com.cidenet.employeeadministration.entity.Country;
import com.cidenet.employeeadministration.entity.Employee;
import com.cidenet.employeeadministration.entity.EmployeeState;
import com.cidenet.employeeadministration.management.ValidationError;
import com.cidenet.employeeadministration.repository.EmployeeRepository;
import io.vavr.control.Either;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Component
@Service
public class EmployeeServiceImpl implements EmployeeService{
    EmployeeRepository _employeeRepository;

    @Autowired
    public void setEmployeeRepository(EmployeeRepository employeeRepository){
        _employeeRepository = employeeRepository;
    }

    @Override
    public Either<List<ValidationError>,Employee> registerEmployee(Employee employee) {
        employee.setId(null);
        employee.setEditingDate(null);

        List<ValidationError> errorsList = validateFullEmployeeInformation(employee);

        if(errorsList.isEmpty()){
           return Either.right(_employeeRepository.save(employee));
        }

        return Either.left(errorsList);
    }

    @Override
    public Either<List<ValidationError>,Employee> editEmployee(Long id, Employee employee) {
        List<ValidationError> validationErrors = new ArrayList<>();
        Employee original = _employeeRepository.findEmployeeById(id);

        if(!(original == null)){
           validationErrors = validateFullEmployeeInformation(employee);
           if(validationErrors.isEmpty()){
               employee.setId(original.getId());
               employee.setEntryDate(original.getEntryDate());
               employee.setState(EmployeeState.ACTIVE);
               employee.setEditingDate(new Date());

               return Either.right(_employeeRepository.save(employee)) ;
           }
        }else{
            validationErrors.add(ValidationError.EMPLOYEE_EXISTS_VALIDATION_ERROR);
        }

        return Either.left(validationErrors);
    }

    @Override
    public List<Employee> showAllEmployees() {
        return _employeeRepository.findAll();
    }

    @Override
    public Either<List<ValidationError>,Employee> deleteEmployeeById(Long id) {
        List<ValidationError> validationErrors = new ArrayList<>();
        Employee original = _employeeRepository.findEmployeeById(id);

        if(!(original == null)){
            int rowsChanged = _employeeRepository.deleteEmployeeById(id);
            if(rowsChanged == 1){
                return Either.right(original);
            }

        }else{
            validationErrors.add(ValidationError.EMPLOYEE_EXISTS_VALIDATION_ERROR);
        }

        return Either.left(validationErrors);
    }

    List<ValidationError> validateFullEmployeeInformation(Employee employee){
        List<ValidationError> validationErrors = new ArrayList<>();

        /*Verify the employee object is not null*/
        if(!(employee == null)){
            boolean isFirstLastNameValid = validateStringCharactersAndSize(
                    employee.getFirstLastName(),20);
            boolean isSecondLastNameValid = validateStringCharactersAndSize(
                    employee.getSecondLastName(),20);
            boolean isFirstNameValid = validateStringCharactersAndSize(
                    employee.getFirstName(),20);

            /*We check first that the other names field has a value since this value can be empty*/
            boolean isOtherNameProvided = false;
            boolean isOtherNameValid = false;
            if(!(employee.getOtherNames() == null) && !employee.getOtherNames().isEmpty()){
                isOtherNameProvided = true;
                isOtherNameValid = validateStringCharactersAndSize(
                        employee.getFirstName(),20);
            }

            /*We validate if the id is valid and after that we validate that there's no duplicates
            * of the id and id type already in the database*/
            boolean isIdValid = validateIdCharactersAndSize(employee.getEmployeeId());
            boolean areIdAndTypeIdUnique = false;

            if (isIdValid) {
                Employee employeeWithSameIdAndTypeId = _employeeRepository.findEmployeeByIdTypeAndEmployeeId(
                        employee.getIdType(), employee.getEmployeeId());

                if (!(employeeWithSameIdAndTypeId == null)) {
                    if(employeeWithSameIdAndTypeId.getId().equals(employee.getId()))
                    {
                        areIdAndTypeIdUnique = true;
                    }
                }else{
                    areIdAndTypeIdUnique = true;
                }
            }

            if(!(employee.getFirstLastName() == null || employee.getFirstName() == null
                    || employee.getEmploymentCountry() == null)){
                String generatedEmail = generateEmail(employee.getFirstName(), employee.getFirstLastName(),
                        employee.getEmploymentCountry());

                employee.setEmail(generatedEmail);
            }

            SimpleDateFormat sdf = new SimpleDateFormat();

            boolean isEntryDateValid = false;
            Date date = new Date();

            if(employee.getEntryDate().before(date) ){
                isEntryDateValid = true;
            }

            /*Here we start validating each element and we add a Validation Error to the
            least for each one that doesn't meet the standars */
            if(!isFirstLastNameValid){
                validationErrors.add(ValidationError.FIRST_LAST_NAME_VALIDATION_ERROR);
            }

            if(!isSecondLastNameValid){
                validationErrors.add(ValidationError.SECOND_LAST_NAME_VALIDATION_ERROR);
            }

            if(!isFirstNameValid){
                validationErrors.add(ValidationError.FIRST_NAME_VALIDATION_ERROR);
            }

            if(isOtherNameProvided){
                if(!isOtherNameValid){
                    validationErrors.add(ValidationError.OTHER_NAME_VALIDATION_ERROR);
                }
            }

            if(!isIdValid){
                validationErrors.add(ValidationError.ID_VALIDATION_ERROR);
            } else if (!areIdAndTypeIdUnique) {
                validationErrors.add(ValidationError.REPEATING_ID_VALIDATION_ERROR);
            }

            if(!isEntryDateValid){
                validationErrors.add(ValidationError.ENTRY_DATE_VALIDATION_ERROR);
            }

        }else{
            validationErrors.add(ValidationError.EMPLOYEE_VALIDATION_ERROR);
        }

        return validationErrors;
    }


    /*This method will help validate that a String is not bigger than the set size and
    * that is only uppercase alphabet (with no accents or special letters)*/
    public boolean validateStringCharactersAndSize(String strToEvaluate,int maxSize){
        boolean isValid = false;

        /*It's important to validate that the string is not empty or null*/
        if(!(strToEvaluate == null) && !strToEvaluate.isEmpty()){

            /*We validate that the length doesn't surpass the max length */
            if( !(strToEvaluate.length()>maxSize) ){

                /*We validate that it only includes letters from A to Z upper case
                * and no special letters or accents*/
                if(strToEvaluate.matches("[A-Z\\s]+")){
                    /*At this point all restrictions are met and we can say the string's format
                    * is okay in any other case we say the string doesn't meet the restrictions*/
                    isValid = true;
                }
            }
        }

        return isValid;
    }

    public boolean validateIdCharactersAndSize(String idToEvaluate){
        boolean isValid = false;

        /*It's important to validate that the string is not empty or null*/
        if(!(idToEvaluate == null) && !idToEvaluate.isEmpty()){

            /*We validate if the length is 20 characters max*/
            if( !(idToEvaluate.length()>20) ){

                /*We validate that it only includes letters from A to Z upper case
                 * and no special letters or accents*/
                if(idToEvaluate.matches("[A-Za-z0-9]+")){
                    /*At this point all restrictions are met and we can say the string's format
                     * is okay in any other case we say the string doesn't meet the restrictions*/
                    isValid = true;
                }
            }
        }

        return isValid;
    }

    public String generateEmail(String firstName, String firstLastName, Country employmentCountry){
        //Remove possible spaces from first name and last name
        String noSpacesLastName = firstLastName.replaceAll("\\s","").toLowerCase();
        String noSpacesFirstName = firstName.replaceAll("\\s","").toLowerCase();

        //Composing the email with the names and domain without the country
        String email = noSpacesFirstName +"."+ noSpacesLastName;

        int employeesWithSameEmail = _employeeRepository.countEmployeeByEmailContaining(email);

        if(employeesWithSameEmail>0){
            String potentialNewEmail = email + employeesWithSameEmail;

            int employeesWithSameNewEmail = _employeeRepository.countEmployeeByEmailContaining(potentialNewEmail);

            while(employeesWithSameNewEmail>0){
                potentialNewEmail = email + (employeesWithSameEmail + 1);
                employeesWithSameNewEmail = _employeeRepository.countEmployeeByEmailContaining(potentialNewEmail);
            }

            email = potentialNewEmail;
        }

        email = email.concat("@cidenet.com.");

        if(employmentCountry.equals(Country.COLOMBIA)){
            email = email.concat("co");
        }
        if(employmentCountry.equals(Country.UNITED_STATES)){
            email = email.concat("us");
        }

        return email;
    }

}

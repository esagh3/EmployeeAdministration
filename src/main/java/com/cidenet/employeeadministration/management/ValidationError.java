package com.cidenet.employeeadministration.management;

public enum ValidationError {
    FIRST_LAST_NAME_VALIDATION_ERROR("The employee's first last name cannot be Null, must not be longer " +
            "than 20 characters and must include only A to Z characters with no accent and no special " +
            "letters",1),
    SECOND_LAST_NAME_VALIDATION_ERROR("The employee's second last name cannot be Null, must not be longer " +
            "than 20 characters and must include only A to Z characters with no accent and no special " +
            "letters",2),
    FIRST_NAME_VALIDATION_ERROR("The employee's first name cannot be Null, must not be longer " +
            "than 20 characters and must include only A to Z characters with no accent and no special " +
            "letters",3),
    OTHER_NAME_VALIDATION_ERROR("The employee's other name must not be longer " +
            "than 50 characters and must include only A to Z characters with no accent and no special " +
            "letters",4),
    ID_VALIDATION_ERROR("The employee's id cannot be Null, must not be longer than 20 characters and must include " +
            "only alpha-numerical characters with no accent and no special letters",5),
    REPEATING_ID_VALIDATION_ERROR("The employee's id and type of id must not repeat",6),
    ENTRY_DATE_VALIDATION_ERROR("Entry date cannot be Null, must not be after the actual date and must not " +
            "be older than 30 days",7),
    EMPLOYEE_VALIDATION_ERROR("The employee registry must not be null",8),
    EMPLOYEE_EXISTS_VALIDATION_ERROR("The employee doesn't exist",9);

    public final String message;
    public final int code;

    ValidationError(String message, int code){
        this.message = message;
        this.code = code;
    }

    public String toString() {
        return "Error: " + this.message +", Code: " + this.code ;
    }

}

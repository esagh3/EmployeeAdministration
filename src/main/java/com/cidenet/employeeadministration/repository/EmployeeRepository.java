package com.cidenet.employeeadministration.repository;

import com.cidenet.employeeadministration.entity.Employee;
import com.cidenet.employeeadministration.entity.IdType;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    /*public List<String> getAllEmployees();
    public Employee insertEmployee(Employee employee);
    public Employee editEmployee(Employee employee);*/

    @Transactional
    int deleteEmployeeById(Long id);

    List<Employee> findAll();
    Employee findEmployeeByIdTypeAndEmployeeId(IdType idType,String employeeId);
    Employee findEmployeeById(Long id);

    int countEmployeeByEmailContaining(String names);


}

package com.cidenet.employeeadministration.entity;

public enum EmployeeState {
    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE");

    private String label;

    EmployeeState(String label){
        this.label = label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}

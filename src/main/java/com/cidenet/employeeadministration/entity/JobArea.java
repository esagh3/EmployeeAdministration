package com.cidenet.employeeadministration.entity;

public enum JobArea {
    MANAGEMENT("Administracion"),
    FINANCING("Financiera"),
    PURCHASE("Compras"),
    INFRASTRUCTURE("Infraestructura"),
    OPERATIONS("Operaciones"),
    HUMAN_RESOURCES("Talento humano"),
    MISCELLANEOUS("Servicios varios");

    private final String label;

    JobArea(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}

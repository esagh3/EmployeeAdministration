package com.cidenet.employeeadministration.entity;

public enum IdType {
    CEDULA_DE_CIUDADANIA("Cedula de ciudadania"),
    CEDULA_DE_EXTRANJERIA("Cédula de Extranjería"),
    PASAPORTE("Pasaporte"),
    PERMISO_ESPECIAL("Permiso Especial");

    private final String label;

    IdType(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}

package com.cidenet.employeeadministration.entity;


import jakarta.persistence.*;
import java.util.Date;
@Entity
@Table(name="EMPLOYEE")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    @Column(name="first_last_name")
    private String firstLastName;

    @Column(name="second_last_name")
    private String secondLastName;

    @Column(name="first_name")
    private String firstName;

    @Column(name="other_name")
    private String otherNames;
    @Enumerated(EnumType.STRING)
    @Column(name="employment_country")
    private Country employmentCountry;
    @Enumerated(EnumType.STRING)
    @Column(name="id_type")
    private IdType idType;
    @Column(name="employee_id")
    private String employeeId;
    @Column(name="email")
    private String email;
    @Enumerated(EnumType.STRING)
    @Column(name="employee_state")
    private EmployeeState state;
    @Column(name="entry_date")
    private Date entryDate;
    @Column(name="editing_date")
    private Date editingDate;
    @Enumerated(EnumType.STRING)
    @Column(name="job_area")
    private JobArea area;



    //Constructor to create edited objects


    public Employee(Long id, String firstLastName, String secondLastName, String firstName,
                    String otherNames, Country employmentCountry, IdType idType, String employeeId,
                    String email, EmployeeState state, Date entryDate, Date editingDate,
                    JobArea area) {
        this.id = id;
        if(!(firstLastName == null)){
            this.firstLastName = firstLastName.toUpperCase();
        }
        if(!(secondLastName == null)){
            this.secondLastName = secondLastName.toUpperCase();
        }
        if(!(firstName == null)){
            this.firstName = firstName.toUpperCase();
        }
        if(!(otherNames==null)){
            this.otherNames = otherNames.toUpperCase();
        }
        this.employmentCountry = employmentCountry;
        this.idType = idType;
        this.employeeId = employeeId;
        this.email = email;
        this.state = EmployeeState.ACTIVE;
        this.entryDate = entryDate;
        this.editingDate = null;
        this.area = area;
    }

    public Employee() {

    }

    public EmployeeState getState() {
        return state;
    }

    public void setState(EmployeeState state) {
        this.state = EmployeeState.ACTIVE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public void setFirstLastName(String firstLastName) {
        if(!(firstLastName == null)){
            this.firstLastName = firstLastName.toUpperCase();
        }
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        if(!(secondLastName == null)){
            this.secondLastName = secondLastName.toUpperCase();
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if(!(firstName == null)){
            this.firstName = firstName.toUpperCase();
        }
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        if(!(otherNames == null)){
            this.otherNames = otherNames.toUpperCase();
        }
    }

    public Country getEmploymentCountry() {
        return employmentCountry;
    }

    public void setEmploymentCountry(Country employmentCountry) {
        this.employmentCountry = employmentCountry;
    }

    public IdType getIdType() {
        return idType;
    }

    public void setIdType(IdType idType) {
        this.idType = idType;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {

        this.entryDate = entryDate;
    }

    public JobArea getArea() {
        return area;
    }

    public void setArea(JobArea area) {
        this.area = area;
    }

    public Date getEditingDate() {
        return editingDate;
    }

    public void setEditingDate(Date editingDate) {
        this.editingDate = editingDate;
    }


}

package com.cidenet.employeeadministration.entity;

import jakarta.persistence.Entity;


public enum Country {
    UNITED_STATES("Estados Unidos"),
    COLOMBIA("Colombia");

    private final String label;

    Country(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return this.label;
    }
}
